
export const removeChildren = (element: Element) => {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
};

export const validateAttribute = (attrs: string[], name: string, oldVal: string, newVal: string) => {
  if (!attrs.includes(name)) {
    return false;
  } else return oldVal !== newVal;
}
