import { validateAttribute } from '../utilities';
import {
  getDefaultVerbPersonExample,
  getOrderedVerbPersons,
  VerbTempusSet
} from '../conjugation.models';

import { styles } from './verb-table-item-css';

const LABEL_ATTR = 'label';
const VALUE_ATTR = 'value';
const TEMPUS_SET_ATTR = 'tempusset'

class VerbTableItem extends HTMLElement {
  private headline: HTMLHeadingElement;
  private value: HTMLElement;
  private table: HTMLTableElement;
  private rowTemplate: HTMLTemplateElement;
  static get observedAttributes() {
    return [ LABEL_ATTR, VALUE_ATTR, TEMPUS_SET_ATTR ];
  }
  constructor() {
    super();

    const shadowRoot = this.attachShadow({mode: 'open'});
    shadowRoot.appendChild(VerbTableItem.getTemplate().content.cloneNode(true));
    this.headline = shadowRoot.querySelector('h2');
    this.value = shadowRoot.querySelector('.single-value');
    this.table = shadowRoot.querySelector('.mdc-data-table__content') as HTMLTableElement;
    this.rowTemplate = shadowRoot.querySelector('#data-row') as HTMLTemplateElement;
  }

  attributeChangedCallback(name: string, oldVal: string, newVal: string) {
    if (!validateAttribute(VerbTableItem.observedAttributes, name, oldVal, newVal)) {
      return;
    }
    switch (name) {
      case LABEL_ATTR:
        this.setHeadline(newVal);
        break;
      case VALUE_ATTR:
        this.setValue(newVal);
        break;
      case TEMPUS_SET_ATTR:
        this.setTempusSet(JSON.parse(newVal) as VerbTempusSet);
        break;
      default:
        break;
    }
  }

  private static getTemplate(): HTMLTemplateElement {
    const template = document.createElement('template');
    template.innerHTML = `
      <style type="text/css">
        ${styles}
        .mdc-data-table {
          width: 100%;
        }
      </style>
      <div class="tempus-section">
        <h2></h2> 
        <div class="single-value"></div>
        <div class="mdc-data-table">
          <div class="mdc-data-table__table-container">
            <table class="mdc-data-table__table">
              <tbody class="mdc-data-table__content">
              </tbody>
            </table>
          </div>        
        </div> 
        <template id="data-row">
          <tr class="mdc-data-table__row">
            <td class="mdc-data-table__cell"></td>
            <td class="mdc-data-table__cell"></td>
          </tr>   
        </template>                                          
      </div>
    `;
    return template;
  }

  private setHeadline(headline: string) {
    this.headline.innerText = headline;
  }

  private setValue(value: string) {
    this.value.innerText = value;
  }

  private setTempusSet(tempusSet: VerbTempusSet) {
    for (const verbPerson of getOrderedVerbPersons()) {
      const form = tempusSet[verbPerson];
      const row = this.rowTemplate.content.cloneNode(true) as HTMLElement;
      const td = row.querySelectorAll("td");
      td[0].textContent = form ? getDefaultVerbPersonExample(verbPerson) : '';
      td[1].textContent = form?.conjugation || '';
      this.table.appendChild(row);
    }
  }
}

customElements.define('spn-verb-table-item', VerbTableItem);
