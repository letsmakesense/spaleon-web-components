import './verb-table-item';
import { validateAttribute } from '../utilities';
import {
  getIndicativeVerbTempora,
  getStaticVerbTempora, getSubjunctiveVerbTempora,
  VerbConjugationSet,
  VerbTempusSet
} from '../conjugation.models';

type Results = {
  [verbId: number]: VerbConjugationSet
}

const INFINITIVE_ATTR = 'infinitive';


class VerbTable extends HTMLElement {
  API_ENDPOINT = 'https://www.spaleon.org/';
  private headline: HTMLHeadingElement;
  private conjTable: HTMLElement;

  static get observedAttributes() {
    return [INFINITIVE_ATTR];
  }

  constructor() {
    super();

    const shadowRoot = this.attachShadow({mode: 'open'});
    shadowRoot.appendChild(VerbTable.getTemplate().content.cloneNode(true));
    this.headline = shadowRoot.querySelector('h1');
    this.conjTable = shadowRoot.querySelector('.conjugation-table');

  }

  async attributeChangedCallback(name: string, oldVal: string, newVal: string) {
    if (!validateAttribute(VerbTable.observedAttributes, name, oldVal, newVal)) {
      return;
    }
    if (newVal === '') {
      this.setHeadline(newVal);
      this.reset()
      return
    }
    console.log(`${name} changed from ${oldVal} to ${newVal}`);
    switch (name) {
      case INFINITIVE_ATTR:
        this.setHeadline(newVal);
        this.reset();
        const results = await this.fetchItems(newVal);
        const verbForms = Object.values(results)[0];
        for (const form of getStaticVerbTempora()) {
          const value = verbForms[form] as string;
          this.conjTable.appendChild(this.createTableElement(form, value));
        }
        for (const form of getIndicativeVerbTempora()) {
          const value = verbForms[form] as VerbTempusSet;
          this.conjTable.appendChild(this.createTableElement(form, undefined, value));
        }
        for (const form of getSubjunctiveVerbTempora()) {
          const value = verbForms[form] as VerbTempusSet;
          this.conjTable.appendChild(this.createTableElement(form, undefined, value));
        }
        break;
      default:
        break;
    }
  }

  private static getTemplate(): HTMLTemplateElement {
    const template = document.createElement('template');
    template.innerHTML = `
        <style type="text/css">
          .conjugation-table {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
          } 
          .conjugation-table > spn-verb-table-item {
            width: var(--spn-verb-table-item-width, 250px);
            padding-right: var(--spn-verb-table-item-padding-right, 12px)
          }          
        </style>
        <h1></h1>
        <div class="conjugation-table"></div>
    `;
    return template;
  }

  private setHeadline(headline: string) {
    this.headline.innerText = headline;
  }

  private createTableElement(label: string, value?: string, tempusSet?: VerbTempusSet) {
    const elem = document.createElement('spn-verb-table-item');
    elem.setAttribute('label', label);
    if (value) {
      elem.setAttribute('value', value);
    } else if (tempusSet) {
      elem.setAttribute('tempusset', JSON.stringify(tempusSet));
    }
    return elem;
  }

  private async fetchItems(query: string): Promise<Results> {
    if (query.length < 2) {
      return [];
    }
    const url = `${this.API_ENDPOINT}/conjugations/verbs/details?tempora=all&infinitives=${query}`;
    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json() as unknown as { items: Results };
    return data.items;
  }

  private reset() {
    this.conjTable.innerHTML = '';
  }

}


customElements.define('spn-verb-table', VerbTable);
