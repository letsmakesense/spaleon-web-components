export enum VerbPerson {
  SG_1 = 'sg_1',
  SG_2 = 'sg_2',
  SG_3 = 'sg_3',
  PL_1 = 'pl_1',
  PL_2 = 'pl_2',
  PL_3 = 'pl_3',
}

export const verbPersonExamples: {[k: string]: string[]} = {
  [VerbPerson.SG_1]: ['yo'],
  [VerbPerson.SG_2]: ['tú'],
  [VerbPerson.SG_3]: ['ella', 'usted', 'él'],
  [VerbPerson.PL_1]: ['nosotros', 'nosotras'],
  [VerbPerson.PL_2]: ['vosotras', 'vosotros'],
  [VerbPerson.PL_3]: ['ellos', 'ustedes', 'ellas'],
}

export enum VerbTempus {
  condicional = 'condicional',
  futuro = 'futuro',
  futuro_subj = 'futuro_subj',
  gerundio = 'gerundio',
  imperativo = 'imperativo',
  imperativo_neg = 'imperativo_neg',
  imperfecto = 'imperfecto',
  imperfecto1_subj = 'imperfecto1_subj',
  imperfecto2_subj = 'imperfecto2_subj',
  infinitivo = 'infinitivo',
  participio = 'participio',
  presente = 'presente',
  presente_subj = 'presente_subj',
  preterito = 'preterito',
}

export interface VerbConjugationForm {
  id: number
  conjugation: string
  irregular: boolean | number
}

export type VerbTempusSet = {
  [person in VerbPerson]: VerbConjugationForm;
};

export type VerbConjugationSet = {
  [tempus in VerbTempus]: VerbTempusSet | string
}

export const getOrderedVerbPersons = () => {
  return [
    VerbPerson.SG_1,
    VerbPerson.SG_2,
    VerbPerson.SG_3,
    VerbPerson.PL_1,
    VerbPerson.PL_2,
    VerbPerson.PL_3,
  ]
}



export const getStaticVerbTempora = () => {
  return [
    VerbTempus.infinitivo,
    VerbTempus.gerundio,
    VerbTempus.participio
  ]
}

export const getIndicativeVerbTempora = () => {
  return [
    VerbTempus.presente,
    VerbTempus.preterito,
    VerbTempus.imperfecto,
    VerbTempus.futuro,
    VerbTempus.condicional,
    VerbTempus.imperativo,
  ]
}

export const getSubjunctiveVerbTempora = () => {
  return [
    VerbTempus.presente_subj,
    VerbTempus.imperfecto1_subj,
  ]
}

export const getDefaultVerbPersonExample = (person: VerbPerson): string => {
  return verbPersonExamples[person][0];
}

/**
 * @param person
 */
export const getRandomVerbPersonExample = (person: VerbPerson): string => {
  const examples = verbPersonExamples[person];
  return examples[Math.round(Math.random() * 10 * examples.length) % examples.length];
}
