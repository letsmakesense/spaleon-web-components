import '@material/mwc-tab-bar';
import '@material/mwc-tab';
import './verb-search';
import './styles.scss';

interface TabContent {
  element: HTMLElement;
  tabIndex: number;
}

const ROOT_SELECTOR = '#spaleon';
const CONTENT_SELECTOR = ROOT_SELECTOR + ' .tab-contents';
const TAB_CONTENT_SELECTOR = CONTENT_SELECTOR + ' > div';

const init = () => {

  const tabBar = document.querySelector('mwc-tab-bar');
  const tabContents = initTabContents(document.querySelectorAll(TAB_CONTENT_SELECTOR));
  tabBar.addEventListener('MDCTabBar:activated', ($event: CustomEvent) => {
    activateTabContent(
      tabContents,
      $event.detail?.index || 0,
      document.querySelector(CONTENT_SELECTOR)
    );
  });

}

const initTabContents = (nodeList: NodeListOf<HTMLElement>): TabContent[] => {
  const arr: TabContent[] = [];
  nodeList.forEach((element, tabIndex) => arr.push({
    element,
    tabIndex
  }));
  arr.forEach((tabContent) => {
    tabContent.element.remove();
  })
  return arr;
}

const activateTabContent = (tabContents: TabContent[], idx: number, parent: HTMLElement) => {
  for (let i = 0; i < tabContents.length; i++) {
    if (i === idx) {
      parent.append(tabContents[i].element);
    } else {
      tabContents[i].element.remove();
    }
  }
}

init();


