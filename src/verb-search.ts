import '@material/mwc-list/mwc-list.js';
import '@material/mwc-list/mwc-list-item.js';
import '@material/mwc-textfield';
import '@material/mwc-button';
import '@material/mwc-list';
import { TextField } from '@material/mwc-textfield';
import { List } from '@material/mwc-list';
import { removeChildren, validateAttribute } from './utilities';
import { SelectedEvent } from '@material/mwc-list/mwc-list-foundation';

import './verb-table/verb-table';


export interface VerbInfinitive {
  id: number;
  infinitive: string;
}

const QUERY_ATTR = 'data-query';
const ENDPOINT_ATTR = 'endpoint';


class VerbSearch extends HTMLElement {
  API_ENDPOINT = 'https://www.spaleon.org/';
  private endpoint = this.API_ENDPOINT;
  private input: TextField;
  private readonly list: List;
  private verbTable: Element;
  static get observedAttributes() {
    return [QUERY_ATTR, ENDPOINT_ATTR];
  }



  constructor() {


    super();

    const shadowRoot = this.attachShadow({mode: 'closed'});
    shadowRoot.appendChild(VerbSearch.getTemplate().content.cloneNode(true));
    this.input = shadowRoot.querySelector('mwc-textfield');
    this.list = shadowRoot.querySelector('mwc-list');
    this.verbTable = shadowRoot.querySelector('spn-verb-table');
    this.input.addEventListener('input', async () => await this.updateSearchResults(this.input.value));
    this.list.addEventListener('selected', (event: SelectedEvent) => {
      if (event.detail.index < 0) {
        return;
      }
      const listItem = this.list.children[event.detail.index.toString()]
      this.verbTable.setAttribute('infinitive', listItem.innerText);
      this.resetSearch();
    })
  }

  async connectedCallback() {
    const url = new URL(location.href);
    const query = url.searchParams.get('verb-query').trim();
    await this.initSearch(query);
  }

  async attributeChangedCallback(name, oldValue, newValue) {
    if (!validateAttribute(VerbSearch.observedAttributes, name, oldValue, newValue)) {
      return;
    }
    switch (name) {
      case QUERY_ATTR:
        this.input.value = newValue;
        await this.updateSearchResults(newValue);
        this.verbTable.setAttribute('infinitive', '');
        break;
      case ENDPOINT_ATTR:
        const apiUrl = new URL(newValue);
        this.endpoint = apiUrl.href;
        break;
      default:
        break;
    }
  }

  private static getTemplate(): HTMLTemplateElement {
    const template = document.createElement('template');
    template.innerHTML = `
        <form id="conj-search-form">
          <mwc-textfield label="infinitive"></mwc-textfield>
        </form>
        <mwc-list></mwc-list>
        <spn-verb-table></spn-verb-table>
    `;
    return template;
  }

  async initSearch(query: string) {
    if (query.search(/^([A-záéó]{2,32})$/) != -1) {
      console.debug(`Searching with query '${query}'`);
      this.input.value = query;
      await this.updateSearchResults(query);
    } else {
      return null;
    }
  }

  async updateSearchResults(query: string): Promise<void> {
    const items = await this.fetchItems(query);
    removeChildren(this.list);
    if (items.length === 1) {
      this.verbTable.setAttribute('infinitive', items[0].infinitive);
    } else {
      for (let idx in items) {
        const item = items[idx];
        this.addListItem(item, +idx === 0);
      }
    }
  }

  private async fetchItems(query: string): Promise<VerbInfinitive[]> {
    if (query.length < 2) {
      return []
    }
    const url = `${this.endpoint}/conjugations/verbs/list?mode=search&query=${query}`;
    const response = await fetch(url, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    const data = await response.json() as unknown as { items: VerbInfinitive[] };
    return data.items;
  }

  private addListItem(item: VerbInfinitive, selected: boolean) {
    this.list.appendChild(
      this.createListItem(item, selected)
    );
  }

  private createListItem(item: VerbInfinitive, selected = false): HTMLElement {
    const listItem = document.createElement('mwc-list-item');
    listItem.innerText = item.infinitive;
    listItem.setAttribute('data-id', item.id.toString());
    if (selected) {
      listItem.setAttribute('activated', selected.toString());
    }
    return listItem;
  }

  private resetSearch(): void {
    this.input.value = '';
    removeChildren(this.list);
  }

}

customElements.define('spn-verb-search', VerbSearch);
