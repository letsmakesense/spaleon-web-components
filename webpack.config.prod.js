module.exports = {
  mode: 'production',
  entry: {
    'spn-verb-search': './src/verb-search.ts',
  },
  resolve: {
    modules: ['node_modules'],
    extensions: [ '.ts', '.tsx', '.js' ]
  },
  module: {
    rules: [
        {
            test: /\.tsx?$/,
            loader: 'ts-loader'
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            // Creates `style` nodes from JS strings
            "style-loader",
            // Translates CSS into CommonJS
            "css-loader",
            // Compiles Sass to CSS
            "sass-loader",
          ],
        },
    ],
  },
};
