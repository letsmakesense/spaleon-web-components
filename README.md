# Spaleon Web Components #

Easy-to-use standard web components to integrate tools for learning Spanish into any website.

### Available Components

| tag | description
------|------------
| spn-verb-search | Autocomplete search for Spanish verbs

### Usage ###

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Spaleon Web Components</title>
</head>
<body>
<!--Integrate tag into your markup-->
<spn-verb-search></spn-verb-search>
<!--Add script file-->
<script src="spn-verb-search.js"></script>
</body>
</html>

```

### Example ###

![Search Box](docs/verb-search-1.png "Search Box")
![Result List](docs/verb-search-2.png "Result List")
