const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: 'development',
  resolve: {
    modules: ['node_modules'],
    extensions: [ '.ts', '.tsx', '.js' ]
  },
  entry: {
    'verb-table-item': './src/verb-table/verb-table-item.scss',
  },
  module: {
    rules: [
        {
            test: /\.tsx?$/,
            loader: 'ts-loader'
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            // Creates `style` nodes from JS strings
            MiniCssExtractPlugin.loader,
            // Translates CSS into CommonJS
            "css-loader",
            // Compiles Sass to CSS
            "sass-loader",
          ],
        },
    ],
  },
  plugins: [

    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ]
};
