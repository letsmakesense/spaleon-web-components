const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  resolve: {
    modules: ['node_modules'],
    extensions: [ '.ts', '.tsx', '.js' ]
  },
  module: {
    rules: [
        {
            test: /\.tsx?$/,
            loader: 'ts-loader'
        },
        {
          test: /\.s[ac]ss$/i,
          use: [
            // Creates `style` nodes from JS strings
            "style-loader",
            // Translates CSS into CommonJS
            "css-loader",
            // Compiles Sass to CSS
            "sass-loader",
          ],
        },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Spaleon Web Components',
      template: './src/index.html'
    }),
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    hot: true,
    compress: false,
    port: 4200,
  },
};
